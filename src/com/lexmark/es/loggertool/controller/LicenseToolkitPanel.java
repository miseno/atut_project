package com.lexmark.es.loggertool.controller;

import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JLabel;
import javax.swing.JOptionPane;

import java.awt.Font;
import javax.swing.JTextField;
import javax.swing.border.TitledBorder;
import javax.swing.UIManager;
import java.awt.Color;
import javax.swing.JButton;
import javax.swing.JFileChooser;
import javax.swing.JTable;
import java.awt.event.ActionListener;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.awt.event.ActionEvent;

public class LicenseToolkitPanel extends JPanel
{
   private JTextField txtIPaddress;
   private JTextField txtDirectory;
   private JTextField txtLicID;
   private JLabel lblStatusMessage;
   private JButton btnRemoveLicense;
   private JButton btnBrowse;
   private JButton btnInstall;
   private JButton btnViewLicenseData;

   /**
    * Create the panel.
    */
   public LicenseToolkitPanel()
   {
      setLayout(null);
      
      JLabel label = new JLabel("Printer IP Address");
      label.setFont(new Font("Tahoma", Font.BOLD, 13));
      label.setBounds(10, 13, 126, 14);
      add(label);
      
      txtIPaddress = new JTextField();
      txtIPaddress.setColumns(10);
      txtIPaddress.setBounds(146, 11, 689, 20);
      add(txtIPaddress);
      
      JPanel panel = new JPanel();
      panel.setLayout(null);
      panel.setBorder(new TitledBorder(UIManager.getBorder("TitledBorder.border"), "Select License file to Install", TitledBorder.LEADING, TitledBorder.TOP, null, new Color(0, 0, 0)));
      panel.setBounds(10, 52, 825, 135);
      add(panel);
      
      txtDirectory = new JTextField();
      txtDirectory.setText("");
      txtDirectory.setEnabled(false);
      txtDirectory.setEditable(false);
      txtDirectory.setColumns(10);
      txtDirectory.setBounds(10, 30, 679, 20);
      panel.add(txtDirectory);
      
      btnBrowse = new JButton("Browse");
      btnBrowse.addActionListener(new ActionListener() {
         public void actionPerformed(ActionEvent e) {
            
            JFileChooser chooser = new JFileChooser();
            chooser.setCurrentDirectory(new java.io.File("\\\\cdev-jupiter\\SoftwareTest\\Projects_N\\Solutions\\eSFLicenses\\2018\\BSS"));
            chooser.setDialogTitle("Select Directory");
            chooser.setFileSelectionMode(JFileChooser.FILES_ONLY);
            
            chooser.setAcceptAllFileFilterUsed(false);
            
            if (chooser.showOpenDialog(null) == JFileChooser.APPROVE_OPTION) { 
               
               txtDirectory.setText( chooser.getSelectedFile().toString() );    
               
               }  
            
         }
      });
      btnBrowse.setBounds(699, 24, 116, 33);
      panel.add(btnBrowse);
      
      JPanel panel_1 = new JPanel();
      panel_1.setLayout(null);
      panel_1.setBorder(new TitledBorder(UIManager.getBorder("TitledBorder.border"), "Enter License ID to be removed", TitledBorder.LEADING, TitledBorder.TOP, null, new Color(0, 0, 0)));
      panel_1.setBounds(0, 211, 835, 98);
      add(panel_1);
      
      txtLicID = new JTextField();
      txtLicID.setText("");
      txtLicID.setColumns(10);
      txtLicID.setBounds(10, 30, 815, 20);
      panel_1.add(txtLicID);
      
      lblStatusMessage = new JLabel("");
      lblStatusMessage.setFont(new Font("Tahoma", Font.BOLD, 13));
      lblStatusMessage.setBounds(10, 364, 825, 25);
      add(lblStatusMessage);
            
            
      btnRemoveLicense = new JButton("Remove License");
      btnRemoveLicense.addActionListener(new ActionListener() {
         public void actionPerformed(ActionEvent e) {
            
            Thread t = new Thread(new Runnable() {
               @Override
               public void run() {
                 lblStatusMessage.setText(""); 
                 lblStatusMessage.setText("Removing License...Please Wait..");
                       ButtonState(false);
                       int respCode = ResponseController.Status( txtIPaddress.getText().toString().trim() );
                       
                       if (!txtIPaddress.getText().toString().trim().equals( "" ))
                       {
                          if (!txtLicID.getText().toString().trim().equals( "" ))
                          {
                          Runtime rt = Runtime.getRuntime();
                          
                          String curlCommand = "curl -XDELETE -F \"id=" + txtLicID.getText().toString().trim() + "\" http://" + txtIPaddress.getText().toString().trim() + "/webservices/vcc/licenses";
                          String s = null;
                          try
                          {
                             Process pr = rt.exec(curlCommand);
                             
                             BufferedReader stdInput = new BufferedReader(new 
                             InputStreamReader(pr.getInputStream()));
                             
                             while ((s = stdInput.readLine()) != null) {
                                 JOptionPane.showMessageDialog( null, s );
                             }
                             txtLicID.setText( "" );
                          }
                          catch( IOException e1 )
                          {
                             // TODO Auto-generated catch block
                             e1.printStackTrace();
                          }  
                          }
                          else
                          {
                             lblStatusMessage.setText("");
                             lblStatusMessage.setText("Please Select a License file to Install");
                          }
                       }
                       else
                       {
                          lblStatusMessage.setText("");
                          lblStatusMessage.setText("Please enter the Device IP Address you wish the license to install");
                       }
                       ButtonState(true);
                       lblStatusMessage.setText("");      
                       lblStatusMessage.setText("License Removal completed...");
               }     
           });
           t.start();
            
         }
      });
      btnRemoveLicense.setBounds(146, 319, 135, 34);
      add(btnRemoveLicense);
      
      btnInstall = new JButton("Install License");
      btnInstall.addActionListener(new ActionListener() {
         public void actionPerformed(ActionEvent e) {
            
            Thread t = new Thread(new Runnable() {
               @Override
               public void run() {
                 lblStatusMessage.setText("");
                 lblStatusMessage.setText("Uploading Printer Information...");
                       ButtonState(false);
                       
                       int respCode = ResponseController.Status( txtIPaddress.getText().toString().trim() );
                       
                       if (!txtIPaddress.getText().toString().trim().equals( "" ))
                       {
                          if (!txtDirectory.getText().toString().trim().equals( "" ))
                          {
                          Runtime rt = Runtime.getRuntime();
                          
                          String curlCommand = "curl --data-binary @\"" + txtDirectory.getText().toString().trim() + "\" http://" + txtIPaddress.getText().toString().trim() + "/webservices/vcc/licenses";
                          String s = null;
                          try
                          {
                             Process pr = rt.exec(curlCommand);
                             
                             BufferedReader stdInput = new BufferedReader(new 
                             InputStreamReader(pr.getInputStream()));
                             
                             while ((s = stdInput.readLine()) != null) {
                                 JOptionPane.showMessageDialog( null, s );
                             }
                          }
                          catch( IOException e1 )
                          {
                             // TODO Auto-generated catch block
                             e1.printStackTrace();
                          }  
                          }
                          else
                          {
                             lblStatusMessage.setText("");
                             lblStatusMessage.setText("Please Select a license file to install");
                          }
                       }
                       else
                       {
                          lblStatusMessage.setText("");
                          lblStatusMessage.setText("Please enter the Device IP Address you wish the license to install");
                       }
                       
                       ButtonState(true);
                       lblStatusMessage.setText("");
                       lblStatusMessage.setText("Uploading completed...");
               }     
           });
           t.start();
            
         }
      });
      btnInstall.setBounds(10, 320, 131, 34);
      add(btnInstall);
      
      btnViewLicenseData = new JButton("View Installed Licenses");
      btnViewLicenseData.setBounds(291, 320, 170, 33);
      add(btnViewLicenseData);
      btnViewLicenseData.addActionListener(new ActionListener() {
         public void actionPerformed(ActionEvent arg0) {
            
           
               
               Thread t = new Thread(new Runnable() {
                  @Override
                  public void run() {
                    lblStatusMessage.setText("");
                    lblStatusMessage.setText("Uploading License Information...");
                          ButtonState(false);
                          
                             
                          
                             licenses lic = new licenses();
                             if (!txtIPaddress.getText().toString().trim().equals( "" ))
                             {
                                lic.parseLicense( txtIPaddress.getText().toString().trim() );
                                ButtonState(true);
                                lblStatusMessage.setText("");
                                lblStatusMessage.setText("Upload complete...");
                             }
                             else
                             {
                                ButtonState(true);
                                lblStatusMessage.setText("");
                                lblStatusMessage.setText("Please Enter IP Address..");
                             }
                          
                         
                  }     
              });
              t.start();
            
         }
      });
      
   }
   
   public void ButtonState(Boolean state)
   {
      btnRemoveLicense.setEnabled( state );
      btnBrowse.setEnabled( state );
      btnViewLicenseData.setEnabled( state );
      btnInstall.setEnabled( state );
      
   }
}
