package com.lexmark.es.loggertool.controller;

import java.awt.EventQueue;

import javax.swing.JFrame;
import org.eclipse.wb.swing.FocusTraversalOnArray;
import java.awt.Component;
import java.awt.GridLayout;
import javax.swing.JButton;
import javax.swing.JComponent;
import javax.swing.JRadioButton;
import javax.swing.JTextArea;
import javax.swing.JToggleButton;
import javax.swing.JTextField;
import javax.swing.JList;
import java.awt.BorderLayout;
import java.awt.FlowLayout;
import java.awt.GridBagLayout;
import java.awt.GridBagConstraints;
import java.awt.Insets;
import com.jgoodies.forms.layout.FormLayout;
import com.jgoodies.forms.layout.ColumnSpec;
import com.jgoodies.forms.layout.RowSpec;
import com.jgoodies.forms.layout.FormSpecs;
import javax.swing.JTabbedPane;
import javax.swing.BoxLayout;
import javax.swing.JPanel;
import javax.swing.JLabel;
import java.awt.Font;
import net.miginfocom.swing.MigLayout;

public class MainActivity
{

   private JFrame frmAppsTestUtility;

   /**
    * Launch the application.
    */
   public static void main( String[] args )
   {
      EventQueue.invokeLater( new Runnable()
      {
         public void run()
         {
            try
            {
               MainActivity window = new MainActivity();
               window.frmAppsTestUtility.setVisible( true );
            }
            catch( Exception e )
            {
               e.printStackTrace();
            }
         }
      } );
   }

   /**
    * Create the application.
    */
   public MainActivity()
   {
      initialize();
   }

   /**
    * Initialize the contents of the frame.
    */
   private void initialize()
   {
      frmAppsTestUtility = new JFrame();
      frmAppsTestUtility.setTitle("Apps Test Utility Tools");
      frmAppsTestUtility.setBounds( 100, 100, 855, 480 );
      frmAppsTestUtility.setResizable(false);
      frmAppsTestUtility.setDefaultCloseOperation( JFrame.EXIT_ON_CLOSE );
      frmAppsTestUtility.getContentPane().setLayout(new BoxLayout(frmAppsTestUtility.getContentPane(), BoxLayout.X_AXIS));
      
      JTabbedPane tabbedPane = new JTabbedPane(JTabbedPane.TOP);
      tabbedPane.setAlignmentY(Component.BOTTOM_ALIGNMENT);
      frmAppsTestUtility.getContentPane().add(tabbedPane);
      EmbeddedUIPanel emsolutions = new EmbeddedUIPanel();
      
      
      JPanel panelInstaller = new JPanel();
      panelInstaller.setAutoscrolls( true );
      tabbedPane.addTab("App & Firmware Installer", null, panelInstaller, null);
           
      JPanel panelEmbeddedSolutions = new JPanel();
      tabbedPane.addTab("Embedded Solutions", null, emsolutions, null);
      panelEmbeddedSolutions.setPreferredSize(emsolutions.getPreferredSize());
      emsolutions.setLayout(null);
            
      JPanel panelLicenseToolkit = new JPanel();
      LicenseToolkitPanel lictoolkit = new LicenseToolkitPanel();
      tabbedPane.addTab("Moja License Toolkit", null, lictoolkit, null);
      panelLicenseToolkit.setPreferredSize(lictoolkit.getPreferredSize());
      
      JPanel panelPerformanceTimer = new JPanel();
      tabbedPane.addTab("Performance Timer", null, panelPerformanceTimer, null);
      
      JPanel panelContinuousLog = new JPanel();
      tabbedPane.addTab("Continous Log", null, panelContinuousLog, null);
      JComponent panel1 = makeTextPanel("Panel 1");
   }

   private JComponent makeTextPanel( String string )
   {
      // TODO Auto-generated method stub
      return null;
   }

}
