package com.lexmark.es.loggertool.controller;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;

import javax.swing.JFrame;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathExpression;
import javax.xml.xpath.XPathExpressionException;
import javax.xml.xpath.XPathFactory;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;


public class licenses {
      
   public void parseLicense(String device_ip) {
      String app_name = "";
      String license_id = "";
      String license_valid = "";
      String license_type = "";
      String[][] data = new String[174][4];
      int arrCtr = 0;
      
      try {
         URL device_url = new URL("http://" + device_ip.trim() + "/webservices/vcc/licenses");
         DocumentBuilderFactory doc_factory =  DocumentBuilderFactory.newInstance();
         DocumentBuilder doc_builder = doc_factory.newDocumentBuilder();
         Document doc = doc_builder.parse(device_url.openStream());
         doc.getDocumentElement().normalize();
               
         NodeList nListFirstLevelChild = this.returnNodeList(doc, "/ListElem/MapElem/child");                  
         System.out.println(nListFirstLevelChild.getLength());
         
         for(int x = 0; x < nListFirstLevelChild.getLength(); x++){
            //Node nNode = nList.item(x);
            //System.out.println("counter " + x);
            
            Element nNode = (Element) nListFirstLevelChild.item(x);
            if(nNode.getAttribute("name").equals("id")){
               //System.out.println("_______________");  
               //System.out.println("Id: " + nNode.getTextContent().trim());
               //app_name = "";
               license_id = nNode.getTextContent().trim();
            }
            else if(nNode.getAttribute("name").equals("isValid")){
               //System.out.println("Is license valid: " + nNode.getTextContent().trim());
               license_valid = nNode.getTextContent().trim();
            }
            else if (nNode.getAttribute("name").equals("license")){
               Boolean isInfoPresent = false;
               String app_license_type = "";
               String appname_in_info = "";
               String appname_in_features = "";
                             
               
               NodeList licenseLevel1 =  nNode.getChildNodes();
               for (int a=0; a<licenseLevel1.getLength(); a++ ){
                  if(licenseLevel1.item(a).getNodeName().equals("MapElem")){
                     NodeList licenseLevel2 = licenseLevel1.item(a).getChildNodes();
                     for(int b=0; b<licenseLevel2.getLength(); b++){
                        if(licenseLevel2.item(b).getNodeName().equals("child")){                         
                           Element elemChild = (Element) licenseLevel2.item(b);
                           //NodeList licenceLevel3 = elemChild.getChildNodes();
                           if(elemChild.getAttribute("name").equals("type")){
                              //System.out.println("Type: " + elemChild.getTextContent().trim());
                              app_license_type = elemChild.getTextContent().trim(); 
                           }
                           else if (elemChild.getAttribute("name").equals("info")){
                              //System.out.println("Info: " + elemChild.getTextContent().trim());
                              isInfoPresent = true;
                              NodeList licenseLevel3a = elemChild.getElementsByTagName("child");                           
                              for(int c=0; c<licenseLevel3a.getLength(); c++){
                                 Element elemChild1 = (Element) licenseLevel3a.item(c);
                                 if(elemChild1.getAttribute("name").equals("name"))
                                    //System.out.println("App Name: " + elemChild1.getTextContent().trim());
                                    appname_in_info = elemChild1.getTextContent().trim();
                              }                             
                           }
                           else if (elemChild.getAttribute("name").equals("features")){
                              isInfoPresent = false;
                              NodeList licenseLevel3b = elemChild.getElementsByTagName("child");                           
                              for(int d=0; d<licenseLevel3b.getLength(); d++){
                                 Element elemChild1a = (Element) licenseLevel3b.item(d);
                                 if(elemChild1a.getAttribute("name").equals("name"))
                                    //System.out.println("App Name: " + elemChild1a.getTextContent().trim());
                                    appname_in_features = elemChild1a.getTextContent().trim();
                              }                                                  
                           }
                        }
                     }        
                     if(isInfoPresent){
                        //System.out.println("App Name: " + appname_in_info);                      
                        app_name = appname_in_info;
                     }else{
                        //System.out.println("App Name: " + appname_in_features);                     
                        app_name = appname_in_features;
                     }
                     //System.out.println("License Type: " + app_license_type);
                     license_type = app_license_type;
                  }
               
               }
               
            }
            
            
            if(!app_name.equals("") && !license_id.equals("") && !license_valid.equals("") && !license_type.equals("")){
//               System.out.println("*****************************");
//               System.out.println("Application Name: " + app_name);
//               System.out.println("License ID: " + license_id);
//               System.out.println("License Valid: " + license_valid );
//               System.out.println("License Type: " + license_type);                    
//               System.out.println("*****************************");
               
               
                  data[arrCtr][0] = app_name;
                  data[arrCtr][1] = license_id;
                  data[arrCtr][2] = license_valid;
                  data[arrCtr][3] = license_type;
                              
               
               
               
               //clear variable for the next license info
               app_name = "";
               license_id ="";
               license_valid ="";
               license_type ="";
               arrCtr ++;
            }
                     
         }
         

      } catch (ParserConfigurationException e) {
         // TODO Auto-generated catch block
         e.printStackTrace();
       
      }     
      catch (MalformedURLException e1) {
         // TODO Auto-generated catch block
         e1.printStackTrace();
         
      } catch (SAXException e) {
         // TODO Auto-generated catch block
         e.printStackTrace();
                 
      } catch (IOException e) {
         // TODO Auto-generated catch block
         e.printStackTrace();
  
      }
      
    //headers for the table
      String[] columns = new String[] {
          "Application Name", "License ID", "License Valid", "License Type"
      };
      
    //create table with data
      JTable table = new JTable(data, columns);
      JFrame frame = new JFrame();
       
      //add the table to the frame
      frame.add(new JScrollPane(table));
       
      frame.setTitle("License Data");
      frame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);       
      frame.pack();
      frame.setVisible(true);
      
   }
   
   private NodeList returnNodeList(Document doc, String xpath) {
      
      XPath x_path = XPathFactory.newInstance().newXPath();
      XPathExpression expr;
      NodeList nList;
      try {
         expr = x_path.compile(xpath);
         nList = (NodeList) expr.evaluate(doc, XPathConstants.NODESET);
         return nList;
      } catch (XPathExpressionException e) {
         // TODO Auto-generated catch block
         e.printStackTrace();
         return null;
      }
   }
   
}
