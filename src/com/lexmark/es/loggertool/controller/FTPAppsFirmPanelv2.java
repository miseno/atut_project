package com.lexmark.es.loggertool.controller;

import javax.swing.JPanel;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JTextField;
import javax.swing.DefaultListModel;
import javax.swing.JButton;
import javax.swing.JFileChooser;

import java.awt.Font;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.List;

import javax.swing.JSeparator;
import javax.swing.JSplitPane;
import javax.swing.border.LineBorder;
import javax.swing.event.ListSelectionListener;

import org.apache.commons.net.ftp.FTP;
import org.apache.commons.net.ftp.FTPClient;

import java.awt.Color;
import java.awt.event.ActionEvent;
import javax.swing.JTable;
import javax.swing.JTabbedPane;

public class FTPAppsFirmPanelv2 extends JPanel implements ActionListener{
	/**
	 * 
	 */
	
	private JList<String> ipList, appList;
	private DefaultListModel<String> listModel, listModel2;
    
	
	private static final long serialVersionUID = 1L;
	private JTextField txtIPAddress;
	private JButton btnAdd, btnReset, btnBrowse_1, btnClear, btnDeploy_1, btnBrowse_2, btnClear_1, btnDeploy_2;
	private String[] columns = {"IP Address","Firmware"};
	private Object[][] data;
	private JFileChooser fcFirmware, fcApps;
	private File dir, file, dirApps; 
	private File[] fileApps;
	private String filename, filenameApps;
	private JLabel lblStatus;
	boolean noError, noErrorApps;
	private JTextField txtFirmwareFile_1;
	private JTabbedPane tabbedPane;
	
	/**
	 * Create the panel.
	 */
	public FTPAppsFirmPanelv2() {
		setLayout(null);
		
		
		fcFirmware = new JFileChooser();
		fcApps = new JFileChooser();
		fcApps.setMultiSelectionEnabled(true);
		
		JLabel lblNewLabel = new JLabel("IP Addresses:");
		lblNewLabel.setBounds(10, 55, 111, 14);
		add(lblNewLabel);
		
		txtIPAddress = new JTextField();
		txtIPAddress.setBounds(10, 78, 641, 20);
		add(txtIPAddress);
		txtIPAddress.setColumns(10);
		
		btnAdd = new JButton("ADD");
		btnAdd.addActionListener(this);
		btnAdd.setBounds(661, 75, 135, 23);
		add(btnAdd);
		
		btnReset = new JButton("RESET");
		btnReset.setEnabled(false);
		btnReset.addActionListener(this);
		btnReset.setBounds(661, 134, 135, 23);
		add(btnReset);
		
		
		JLabel lblInstallOrUpdate = new JLabel("Install Apps or Firmware");
		lblInstallOrUpdate.setFont(new Font("Tahoma", Font.PLAIN, 14));
		lblInstallOrUpdate.setBounds(10, 11, 312, 20);
		add(lblInstallOrUpdate);
		
		JSeparator separator = new JSeparator();
		separator.setBounds(10, 42, 786, 2);
		add(separator);
		
		JLabel lblListIP = new JLabel("List of IP Addresses");
		lblListIP.setBounds(10, 109, 187, 14);
		add(lblListIP);
		
		listModel = new DefaultListModel<>();
		listModel2 = new DefaultListModel<>();
		 
		//create the list
		ipList = new JList<>(listModel);
		ipList.setVisibleRowCount(11);
		ipList.setLayoutOrientation(JList.HORIZONTAL_WRAP);
		ipList.setBorder(new LineBorder(new Color(0, 0, 0)));
		ipList.setLocation(10, 136);
		add(ipList);
		
		ipList.setSize(161,191);
		ipList.setVisible(true);
		
		lblStatus = new JLabel("Status...");
		lblStatus.setBounds(10, 338, 641, 20);
		add(lblStatus);
		
		tabbedPane = new JTabbedPane(JTabbedPane.TOP);
		tabbedPane.setBounds(181, 134, 471, 193);
		add(tabbedPane);
		
		JPanel pnlApps = new JPanel();
		tabbedPane.addTab("Apps", null, pnlApps, null);
		pnlApps.setLayout(null);
		
		btnBrowse_2 = new JButton("BROWSE");
		btnBrowse_2.setEnabled(false);
		btnBrowse_2.setBounds(10, 11, 89, 23);
		btnBrowse_2.addActionListener(this);
		pnlApps.add(btnBrowse_2);
		
		btnClear_1 = new JButton("CLEAR");
		btnClear_1.setEnabled(false);
		btnClear_1.addActionListener(this);
		btnClear_1.setBounds(367, 11, 89, 23);
		pnlApps.add(btnClear_1);
		
		appList = new JList<>(listModel2);
		appList.setBorder(new LineBorder(new Color(0, 0, 0)));
		appList.setBounds(10, 45, 446, 112);
		pnlApps.add(appList);
		
		btnDeploy_2 = new JButton("DEPLOY");
		btnDeploy_2.setEnabled(false);
		btnDeploy_2.addActionListener(this);
		btnDeploy_2.setBounds(109, 11, 89, 23);
		pnlApps.add(btnDeploy_2);
		
		JPanel pnlFirmware = new JPanel();
		tabbedPane.addTab("Firmware", null, pnlFirmware, null);
		pnlFirmware.setLayout(null);
		
		JLabel lblFirmwareFilefls = new JLabel("Firmware File (FLS): ");
		lblFirmwareFilefls.setBounds(10, 46, 115, 22);
		pnlFirmware.add(lblFirmwareFilefls);
		
		btnBrowse_1 = new JButton("BROWSE");
		btnBrowse_1.setEnabled(false);
		btnBrowse_1.addActionListener(this);
		btnBrowse_1.setBounds(10, 12, 89, 23);
		pnlFirmware.add(btnBrowse_1);
		
		btnClear = new JButton("CLEAR");
		btnClear.setEnabled(false);
		btnClear.addActionListener(this);
		btnClear.setBounds(367, 12, 89, 23);
		pnlFirmware.add(btnClear);
		
		txtFirmwareFile_1 = new JTextField();
		txtFirmwareFile_1.setBounds(119, 47, 337, 20);
		pnlFirmware.add(txtFirmwareFile_1);
		txtFirmwareFile_1.setColumns(10);
		
		btnDeploy_1 = new JButton("DEPLOY");
		btnDeploy_1.setEnabled(false);
		btnDeploy_1.addActionListener(this);
		btnDeploy_1.setBounds(109, 12, 89, 23);
		pnlFirmware.add(btnDeploy_1);
	

	}
	
	public void actionPerformed(ActionEvent e) {
		String ipAddresses;
		String[] ipAddress;
		int size;
		
		if(e.getSource() == btnAdd) {
			ipAddresses = txtIPAddress.getText();
			
			ipAddress = ipAddresses.split(";");
			
			for(String ip:ipAddress) {
				listModel.addElement(ip);
			}
			
			size = listModel.getSize();
			
			if(size > 0) {
				enable(true);
			}
			
			//System.out.println(listModel.get(1).toString());
			//System.out.println(Integer.toString(size));
			clear();
			
		} else if(e.getSource() == btnReset) {
			
			listModel.clear();
			
			clearFirmware();
			clearApps();
			
			size = listModel.getSize();
			
			if(size == 0) {
				enable(false);
			}
			
		} else if(e.getSource() == btnBrowse_1) {
			browse(btnBrowse_1, txtFirmwareFile_1);
		} else if(e.getSource() == btnBrowse_2) {
			browse(btnBrowse_1, listModel2);
		} else if(e.getSource() == btnClear) {
			clearFirmware();
		} else if(e.getSource() == btnClear_1) {
			clearApps();
		} else if(e.getSource() == btnDeploy_1) {
			
			deployFirmware();
			
		} else if(e.getSource() == btnDeploy_2) {
			
			deployApps();
			
		}
		
	}
	
	public void clear() {
		txtIPAddress.setText("");
		
		
	}
	
	public void clearApps() {
		listModel2.clear();
		btnBrowse_2.setEnabled(false);
		btnDeploy_2.setEnabled(false);
		btnClear_1.setEnabled(false);
	}
	
	public void clearFirmware() {
		txtFirmwareFile_1.setText("");
		btnBrowse_1.setEnabled(false);
		btnDeploy_1.setEnabled(false);
		btnClear.setEnabled(false);
	}
	
	public void enable(boolean state) {
		btnReset.setEnabled(state);
		//btnDeploy.setEnabled(state);
		
		btnBrowse_1.setEnabled(state);
		btnDeploy_1.setEnabled(state);
		btnClear.setEnabled(state);
		
		btnBrowse_2.setEnabled(state);
		btnDeploy_2.setEnabled(state);
		btnClear_1.setEnabled(state);
		
		
		
	}
	
	//Firmware
	public void browse(JButton btnBrowse, JTextField txtFirmwareFile) {
		int returnVal = fcFirmware.showOpenDialog(btnBrowse);
		
		if (returnVal == JFileChooser.APPROVE_OPTION) {
			dir = fcFirmware.getCurrentDirectory();
			file = fcFirmware.getSelectedFile();
			filename = dir.getAbsolutePath().toString().concat("\\").concat(file.getName().toString());
			txtFirmwareFile.setText(filename);
		}
	}
	
	//Apps
	public void browse(JButton btnBrowse, DefaultListModel<String> listModel2) {
		int returnVal = fcApps.showOpenDialog(btnBrowse);
		
		if (returnVal == JFileChooser.APPROVE_OPTION) {
			dirApps = fcApps.getCurrentDirectory();
			fileApps = fcApps.getSelectedFiles();
			
			//filenameApps = dirApps.getAbsolutePath().toString().concat("\\").concat(fileApps.getName().toString());
			//listModel2.addElement(filenameApps);
			
			for(int i=0; i<fileApps.length; i++) {
				final File theFile = fileApps[i];
				
				listModel2.addElement(dirApps.getAbsolutePath().toString().concat("\\").concat(theFile.getName().toString()));
			}
			
			
		}
	}
	
	
	public void deployFirmware() {
		//Use threading to set the label
		Thread t = new Thread(new Runnable() {
	        @Override
	        public void run() {
	        	
	        	enable(false);
	        	
	            //Do the job
	        	FTPClient ftpClient = new FTPClient();
				
				try {
					
					//String machine = new String(txtMachineAddress.getText().toString());
					String machine;
					for(int i=0; i<listModel.getSize(); i++) {
						
					
						machine = listModel.get(i).toString();
						
						lblStatus.setText("Start uploading the file ("+machine+")");
						
						ftpClient.connect(machine, 21);	
						ftpClient.login("", "");
						ftpClient.enterLocalPassiveMode();
						
						ftpClient.setFileType(FTP.BINARY_FILE_TYPE);
						
						File firstLocalFile = new File(filename.toString());
						String firstRemoteFile = file.toString();
						InputStream inputStream = new FileInputStream(firstLocalFile);
						
						OutputStream outputStream = ftpClient.storeFileStream(firstRemoteFile);
						byte[] bytesIn = new byte[4096];
			            int read = 0;
			 
			            while ((read = inputStream.read(bytesIn)) != -1) {
			                outputStream.write(bytesIn, 0, read);
			            }
			            inputStream.close();
			            outputStream.close();
			 
			            boolean completed = ftpClient.completePendingCommand();
			            if (completed) {
			            	noError = true;
			            } else {
			            	noError = false;
			            }
		            
					}
					
				} catch (IOException ex) {
		            System.out.println("Error: " + ex.getMessage());
		            ex.printStackTrace();
		        } finally {
		            try {
		                if (ftpClient.isConnected()) {
		                    ftpClient.logout();
		                    ftpClient.disconnect();
		                }
		            } catch (IOException ex) {
		                ex.printStackTrace();
		            }
		        }
	            
				if(noError) {
					lblStatus.setText("Uploading complete please wait for the printer to restart.");
	            	enable(true);
				}
	        }     
	    });
	    t.start();
	}
	
	public void deployApps() {
		//Use threading to set the label
		Thread t = new Thread(new Runnable() {
	        @Override
	        public void run() {
	        	
	        	enable(false);
	        	
	            //Do the job
	        	FTPClient ftpClient = new FTPClient();
				
				try {
					
				
					String machine;
					String appname;
					for(int i=0; i<listModel.getSize(); i++) {
						
					
						machine = listModel.get(i).toString();
						
						lblStatus.setText("Start uploading the file ("+machine+")");
						
						ftpClient.connect(machine, 21);	
						ftpClient.login("", "");
						ftpClient.enterLocalPassiveMode();
						
						ftpClient.setFileType(FTP.BINARY_FILE_TYPE);
						
						for (int x=0; x<listModel2.getSize(); x++) {
						
							appname = listModel2.get(x).toString();
							
							File firstLocalFile = new File(appname);
							String firstRemoteFile = fileApps.toString();
							InputStream inputStream = new FileInputStream(firstLocalFile);
							
							OutputStream outputStream = ftpClient.storeFileStream(firstRemoteFile);
							byte[] bytesIn = new byte[4096];
				            int read = 0;
				 
				            while ((read = inputStream.read(bytesIn)) != -1) {
				                outputStream.write(bytesIn, 0, read);
				            }
				            inputStream.close();
				            outputStream.close();
				 
				            boolean completed = ftpClient.completePendingCommand();
				            if (completed) {
				            	noErrorApps = true;
				            } else {
				            	noErrorApps = false;
				            }
						}
		            
					}
					
				} catch (IOException ex) {
		            System.out.println("Error: " + ex.getMessage());
		            ex.printStackTrace();
		        } finally {
		            try {
		                if (ftpClient.isConnected()) {
		                    ftpClient.logout();
		                    ftpClient.disconnect();
		                }
		            } catch (IOException ex) {
		                ex.printStackTrace();
		            }
		        }
	            
				if(noErrorApps) {
					lblStatus.setText("Uploading completed...");
	            	enable(true);
				}
	        }     
	    });
	    t.start();
	}
}
