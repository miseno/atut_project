package com.lexmark.es.loggertool.controller;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;

import javax.script.ScriptException;

public class Clear
{
   public final static void ClearSELOG(String ipAddress) throws ScriptException
   {
      try {
         
         String response = ResponseController.FamilyClassify( ipAddress );
         
         String Moja = "http://" + ipAddress + "/esf/prtappse/semenu?clearlogconfirmation=Yes&page=clearlog"; 
         String Legacy = "http://" + ipAddress + "/cgi-bin/direct/printer/prtappse/semenu?clearlogconfirmation=Yes&page=clearlog"; 
         
         
         if (response == "MOJA")
         {
            URL url = new URL(Moja);
            URLConnection conn = url.openConnection();
            
            // open the stream and put it into BufferedReader
            BufferedReader br = new BufferedReader(new InputStreamReader(conn.getInputStream()));
            
            String inputLine = br.readLine();
         }
         
         else if (response == "LEGACY")
         {
            URL url = new URL(Legacy);
            URLConnection conn = url.openConnection();
            
            // open the stream and put it into BufferedReader
            BufferedReader br = new BufferedReader(new InputStreamReader(conn.getInputStream()));
            
            String inputLine = br.readLine();
         }
         
      } catch (MalformedURLException e) {
         e.printStackTrace();
      } catch (IOException e) {
         e.printStackTrace();
      }
   }
  
}
