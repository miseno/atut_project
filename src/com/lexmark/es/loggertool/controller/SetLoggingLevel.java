package com.lexmark.es.loggertool.controller;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;

import javax.script.ScriptException;

public class SetLoggingLevel
{
   public final static void logYes(String ipAddress) throws ScriptException
   {
      try {
         
         String response = ResponseController.FamilyClassify( ipAddress );
                  
         String siteMoja = "http://" + ipAddress + "/esf/prtappse/semenu?loglevel=debug&page=setloglevel";
         String siteLegacy = "http://" + ipAddress + "/cgi-bin/direct/printer/prtappse/semenu?loglevel=debug&page=setloglevel";
           
         if (response == "MOJA")
         {
            URL url = new URL(siteMoja);
            URLConnection conn = url.openConnection();
            
            
            // open the stream and put it into BufferedReader
            BufferedReader br = new BufferedReader(new InputStreamReader(conn.getInputStream()));
            
            String inputLine = br.readLine();
         }
         else if (response == "LEGACY")
         {
            URL url = new URL(siteLegacy);
            URLConnection conn = url.openConnection();
            
            
            // open the stream and put it into BufferedReader
            BufferedReader br = new BufferedReader(new InputStreamReader(conn.getInputStream()));
            
            String inputLine = br.readLine();
         }
             
                 
      } catch (MalformedURLException e) {
         e.printStackTrace();
      } catch (IOException e) {
         e.printStackTrace();
      }
   }
   
   public final static void logNo(String ipAddress) throws ScriptException
   {
      try {
         
         String response = ResponseController.FamilyClassify( ipAddress );
         
         String siteMoja = "http://" + ipAddress + "/esf/prtappse/semenu?loglevel=info&page=setloglevel";
         String siteLegacy = "http://" + ipAddress + "/cgi-bin/direct/printer/prtappse/semenu?loglevel=info&page=setloglevel";
           
         if (response == "MOJA")
         {
            URL url = new URL(siteMoja);
            URLConnection conn = url.openConnection();
            
            
            // open the stream and put it into BufferedReader
            BufferedReader br = new BufferedReader(new InputStreamReader(conn.getInputStream()));
            
            String inputLine = br.readLine();
         }
         else if (response == "LEGACY")
         {
            URL url = new URL(siteLegacy);
            URLConnection conn = url.openConnection();
            
            
            // open the stream and put it into BufferedReader
            BufferedReader br = new BufferedReader(new InputStreamReader(conn.getInputStream()));
            
            String inputLine = br.readLine();
         }
             
                 
      } catch (MalformedURLException e) {
         e.printStackTrace();
      } catch (IOException e) {
         e.printStackTrace();
      }
   }
   
   public final static void logDefault(String ipAddress) throws ScriptException
   {
      try {
         
         String response = ResponseController.FamilyClassify( ipAddress );
         
         String siteMoja = "http://" + ipAddress + "/esf/prtappse/semenu?loglevel=default&page=setloglevel";
         String siteLegacy = "http://" + ipAddress + "/cgi-bin/direct/printer/prtappse/semenu?loglevel=default&page=setloglevel";
           
         if (response == "MOJA")
         {
            URL url = new URL(siteMoja);
            URLConnection conn = url.openConnection();
            
            
            // open the stream and put it into BufferedReader
            BufferedReader br = new BufferedReader(new InputStreamReader(conn.getInputStream()));
            
            String inputLine = br.readLine();
         }
         else if (response == "LEGACY")
         {
            URL url = new URL(siteLegacy);
            URLConnection conn = url.openConnection();
            
            
            // open the stream and put it into BufferedReader
            BufferedReader br = new BufferedReader(new InputStreamReader(conn.getInputStream()));
            
            String inputLine = br.readLine();
         }
             
                 
      } catch (MalformedURLException e) {
         e.printStackTrace();
      } catch (IOException e) {
         e.printStackTrace();
      }
   }
}
