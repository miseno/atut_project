package com.lexmark.es.loggertool.controller;

import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.script.ScriptException;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import javax.swing.JLabel;
import java.awt.Font;
import javax.swing.JTextField;
import java.awt.Toolkit;

public class ContinuousCaptureUI
{

   public JFrame frmContSELogger;
   private JTextField txtIPAddress;

   /**
    * Launch the application.
    */
   public static void main( String[] args )
   {
      EventQueue.invokeLater( new Runnable()
      {
         public void run()
         {
            try
            {
               ContinuousCaptureUI window = new ContinuousCaptureUI();
               window.frmContSELogger.setVisible( true );
            }
            catch( Exception e )
            {
               e.printStackTrace();
            }
         }
      } );
   }

   /**
    * Create the application.
    */
   public ContinuousCaptureUI()
   {
      initialize();
   }

   /**
    * Initialize the contents of the frame.
    */
   private void initialize()
   {
      frmContSELogger = new JFrame();
      frmContSELogger.setIconImage(Toolkit.getDefaultToolkit().getImage("C:\\Users\\miseno\\Downloads\\images\\images.png"));
      frmContSELogger.setTitle("SE Logs Continuous Capture Tool");
      frmContSELogger.setBounds( 100, 100, 345, 139 );
      frmContSELogger.setDefaultCloseOperation( JFrame.DISPOSE_ON_CLOSE );
      frmContSELogger.getContentPane().setLayout(null);
      
      txtIPAddress = new JTextField();
      txtIPAddress.setColumns(10);
      txtIPAddress.setBounds(98, 11, 219, 20);
      frmContSELogger.getContentPane().add(txtIPAddress);
      
      JButton btnStartCapture = new JButton("Start Capture");
      btnStartCapture.addActionListener(new ActionListener() {
         public void actionPerformed(ActionEvent arg0) {
            try
            {
               Clear.ClearSELOG( txtIPAddress.getText().toString().trim() );
            }
            catch( ScriptException e )
            {
               // TODO Auto-generated catch block
               e.printStackTrace();
            }
            new ContCapture(2, txtIPAddress.getText().toString().trim());
         }
      });
      btnStartCapture.setBounds(5, 66, 112, 23);
      frmContSELogger.getContentPane().add(btnStartCapture);
      
      JLabel label = new JLabel("IP ADDRESS");
      label.setFont(new Font("Tahoma", Font.BOLD, 13));
      label.setBounds(10, 13, 81, 14);
      frmContSELogger.getContentPane().add(label);     
      
      
      JLabel lblRunningTime = new JLabel("Running Time");
      lblRunningTime.setFont(new Font("Tahoma", Font.BOLD, 13));
      lblRunningTime.setBounds(10, 38, 107, 23);
      frmContSELogger.getContentPane().add(lblRunningTime);
      
      JButton btnStopCapture = new JButton("Stop Capture");
      btnStopCapture.addActionListener(new ActionListener() {
         public void actionPerformed(ActionEvent arg0) {
         }
      });
      btnStopCapture.setBounds(205, 66, 112, 23);
      frmContSELogger.getContentPane().add(btnStopCapture);
   }
}
