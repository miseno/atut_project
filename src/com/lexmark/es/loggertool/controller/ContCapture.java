package com.lexmark.es.loggertool.controller;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.DataInputStream;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.util.Timer;
import java.util.TimerTask;

public class ContCapture
{
   Timer timer;
   String ipAddress;
   
   public ContCapture(int seconds, String ip)
   {
      timer = new Timer();      
      timer.scheduleAtFixedRate(new APICallerTask(), 2000, 2000);      
      ipAddress = ip;
   }
   
   public void stop()
   {
      timer.cancel();
   }
   
   class APICallerTask extends TimerTask
   {
                           @Override
                           public void run()
                           {
                              // TODO Auto-generated method stub         
                              try {            
                                 String site = "http://" + ipAddress + "/cgi-bin/prtapplog";
                                             
                                 // get URL content
                                URL url = new URL(site);
                                 URLConnection conn = url.openConnection();
                                 
                                 // open the stream and put it into BufferedReader
                                 BufferedReader br = new BufferedReader(new InputStreamReader(conn.getInputStream()));
                                 
                                 String inputLine = br.readLine();
                                                     
                                 //save to this filename
                                 String fileName = "D:/LOGS/" + "SE LOGS.txt";
                                 File file = new File(fileName);
                     
                                 if (!file.exists()) {
                                    file.createNewFile();
                                 }
                                 
                                 FileWriter fw = new FileWriter(file);
                                 BufferedWriter bw = new BufferedWriter(fw);
                                 
                                                    
                                 while ((inputLine = br.readLine()) != null) {
                                 bw.append( inputLine );
                                 bw.newLine();
                                 }
                     
                                 bw.close();
                                 br.close();
                                 
                              } catch (MalformedURLException e) {
                                 e.printStackTrace();
                              } catch (IOException e) {
                                 e.printStackTrace();
                              }
                              
                           }                         
            
      
   }
}
