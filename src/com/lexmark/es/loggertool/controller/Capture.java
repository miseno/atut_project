package com.lexmark.es.loggertool.controller;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.util.Date;

import javax.script.ScriptException;

import com.ibm.icu.text.SimpleDateFormat;
import com.ibm.icu.util.TimeZone;

public class Capture
{
   
   public final static String ShowInfo(String ipAddress) throws Exception
   {
      String devInfo = "http://" + ipAddress + "/cgi-bin/history";
      String info = "";
      String model = "";
      String serial = "";
      String fwRev = "";
      try
      {
         URL devInfoUrl = new URL (devInfo);
         URLConnection devInfoConn = devInfoUrl.openConnection();
         
         // open the stream and put it into BufferedReader
         BufferedReader brdevInfo = new BufferedReader(new InputStreamReader(devInfoConn.getInputStream()));
         
         String inputLinedevInfo = brdevInfo.readLine();
         
         while ((inputLinedevInfo = brdevInfo.readLine()) != null )
         {
            if(inputLinedevInfo.contains( "Model Name" ))
            {
               model = inputLinedevInfo;
            }
            if(inputLinedevInfo.contains( "Serial Number" ))
            {
               serial = inputLinedevInfo;
            }
            if(inputLinedevInfo.contains( "Firmware Revision" ))
            {
               fwRev = inputLinedevInfo;
            }
         }
      }
      catch( MalformedURLException e )
      {
         // TODO Auto-generated catch block
         e.printStackTrace();
      }
      
      info = model + "\n"+ serial + "\n"+ fwRev;
      
      return info;
   }
   
      
   public final static void Save(String ipAddress, String fname, String dir) throws ScriptException
   {
      try {
         
         String site = "http://" + ipAddress + "/cgi-bin/prtapplog";
         String devInfo = "http://" + ipAddress + "/cgi-bin/history";
                     
         // get URL content
        URL url = new URL(site);
        URL devInfoUrl = new URL (devInfo);
        
         URLConnection devInfoConn = devInfoUrl.openConnection();
         URLConnection conn = url.openConnection();
         
         // open the stream and put it into BufferedReader
         BufferedReader br = new BufferedReader(new InputStreamReader(conn.getInputStream()));
         BufferedReader brdevInfo = new BufferedReader(new InputStreamReader(devInfoConn.getInputStream()));
         
         String inputLine = br.readLine();
         
         String inputLinedevInfo = brdevInfo.readLine();
                 
         //save to this filename
         String fileName = dir + "/" + "SE LOGS.txt";
         File file = new File(fileName);

         if (!file.exists()) {
            file.createNewFile();
         }
         
         FileWriter fw = new FileWriter(file);
         BufferedWriter bw = new BufferedWriter(fw);
         
         while ((inputLinedevInfo = brdevInfo.readLine()) != null )
         {
            if(inputLinedevInfo.contains( "Model Name" ))
            {
            bw.write("------------------ Device Info ------------------------");
            bw.newLine();
               
            bw.write(inputLinedevInfo);
            bw.newLine();
            }
            if(inputLinedevInfo.contains( "Serial Number" ))
            {
            bw.write(inputLinedevInfo);
            bw.newLine();
            }
            if(inputLinedevInfo.contains( "Firmware Revision" ))
            {
            bw.write(inputLinedevInfo);
            bw.newLine();
            
            bw.write("-------------------------------------------------------");
            bw.newLine();
            }
         }
                 
         while ((inputLine = br.readLine()) != null) {
         bw.write(inputLine);
         bw.newLine();
//         bw.append( inputLine );
//         bw.newLine();
         }

         bw.close();
         br.close();
         
      } catch (MalformedURLException e) {
         e.printStackTrace();
      } catch (IOException e) {
         e.printStackTrace();
      }
   }
   
   public final static void BundleInfo(String ipAddress, String fname, String dir) throws ScriptException
   {
      try {
         
         String site = "http://" + ipAddress + "/esf/prtappse/semenu?page=bundles";
                
         
         // get URL content
        URL url = new URL(site);
         URLConnection conn = url.openConnection();
         
         // open the stream and put it into BufferedReader
         BufferedReader br = new BufferedReader(new InputStreamReader(conn.getInputStream()));

         String inputLine = br.readLine();
                  
         //save to this filename
         String fileName = dir + "/" + " Bundle Info.txt";
         File file = new File(fileName);

         if (!file.exists()) {
            file.createNewFile();
         }
         
         FileWriter fw = new FileWriter(file);
         BufferedWriter bw = new BufferedWriter(fw);
         
         while ((inputLine = br.readLine()) != null) {
         bw.write(inputLine);
         bw.newLine();
         }

         bw.close();
         br.close();
         
      } catch (MalformedURLException e) {
         e.printStackTrace();
      } catch (IOException e) {
         e.printStackTrace();
      }
   }
   
      
   public final static void getFirmwareDebug(String ipAddress, String dir) throws ScriptException
   {
              
         String site = "http://" + ipAddress + "/cgi-bin/collect-selogs-cgi ";
                 
         try {
             HttpDownloadUtility.downloadFile(site, dir);
         } catch (IOException ex) {
             ex.printStackTrace();
         }             
   }
   
   private final static String getDateTime()
   {
       SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd.hh.mm.ss");
       df.setTimeZone(TimeZone.getTimeZone( "CST" ));
       return df.format(new Date());
   }
   
   public static String dirCreate(String fName, String dir)
   {
      String directory = null;
      
      String DateTime = getDateTime().toString();
      String combine = fName + "(" + DateTime + ")";
      new File(dir.replace("\\", "/") + "/" + combine).mkdirs();
      
      directory = dir + "\\" + combine;
                  
      return directory;
   }
   
}
