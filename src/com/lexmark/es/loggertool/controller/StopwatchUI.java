package com.lexmark.es.loggertool.controller;

import java.awt.Color;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;

import java.awt.Font;

import javax.script.ScriptException;
import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JComponent;

import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.sql.Date;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;
import java.awt.event.ActionEvent;
import javax.swing.JTextField;
import javax.swing.SwingWorker;
import javax.swing.border.Border;

import com.ibm.icu.text.DecimalFormat;
import com.ibm.icu.text.SimpleDateFormat;
import com.ibm.icu.util.TimeZone;

import javax.swing.SwingConstants;
import javax.swing.JToggleButton;
import javax.swing.KeyStroke;

import java.awt.Toolkit;
import javax.swing.JTextArea;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.border.TitledBorder;
import javax.swing.JRadioButton;
import javax.swing.JScrollPane;
import javax.swing.UIManager;
import javax.swing.border.LineBorder;

public class StopwatchUI
{

   public static JFrame frmPerformanceTimer;
   private JButton btnStop;
   private boolean stopped;
   String label;
   int hours = 0;
   int minutes = 0;
   int seconds = 0;
   int ms = 0;
   int counter = 0;
   long resultMin = 0;
   List<Long> content = new ArrayList<Long>();
   Border border = BorderFactory.createLineBorder(Color.BLUE, 5);
   private JTextField txtAverage;
   private JTextField txtTotal;

   /**
    * Launch the application.
    */
   public static void main( String[] args )
   {
      EventQueue.invokeLater( new Runnable()
      {
         public void run()
         {
            try
            {
               StopwatchUI window = new StopwatchUI();
               window.frmPerformanceTimer.setVisible( true );
            }
            catch( Exception e )
            {
               e.printStackTrace();
            }
         }
      } );
   }

   /**
    * Create the application.
    */
   public StopwatchUI()
   {
      initialize();      
   }

   /**
    * Initialize the contents of the frame.
    */
   private void initialize()
   {
      frmPerformanceTimer = new JFrame();
      frmPerformanceTimer.setResizable(false);
      frmPerformanceTimer.setTitle("Performance Timer");
      frmPerformanceTimer.setIconImage(Toolkit.getDefaultToolkit().getImage("C:\\Users\\miseno\\Downloads\\images\\images.png"));
      frmPerformanceTimer.setBounds( 100, 100, 508, 381 );
      frmPerformanceTimer.setDefaultCloseOperation( JFrame.DISPOSE_ON_CLOSE );
      frmPerformanceTimer.getContentPane().setLayout(null);
      frmPerformanceTimer.setLocationRelativeTo( null );
      
      JPanel panel = new JPanel();
      panel.setLayout(null);
      panel.setBorder(new TitledBorder(UIManager.getBorder("TitledBorder.border"), "Laps", TitledBorder.LEADING, TitledBorder.TOP, null, new Color(0, 0, 0)));
      panel.setBounds(128, 100, 163, 232);
      frmPerformanceTimer.getContentPane().add(panel);
      
      JScrollPane scrollPane = new JScrollPane();
      scrollPane.setBounds(10, 21, 145, 169);
      panel.add(scrollPane);
      
      final JTextArea txtLap = new JTextArea();
      scrollPane.setViewportView(txtLap);
      txtLap.setEditable(false);
      
      JLabel lblTotal = new JLabel("Total:");
      lblTotal.setBounds(10, 204, 36, 14);
      panel.add(lblTotal);
      
      txtTotal = new JTextField();
      txtTotal.setEditable(false);
      txtTotal.setColumns(10);
      txtTotal.setBounds(51, 201, 104, 20);
      panel.add(txtTotal);
      
            
      JPanel panel_1 = new JPanel();
      panel_1.setLayout(null);
      panel_1.setBorder(new TitledBorder(UIManager.getBorder("TitledBorder.border"), "Results", TitledBorder.LEADING, TitledBorder.TOP, null, new Color(0, 0, 0)));
      panel_1.setBounds(301, 100, 175, 232);
      frmPerformanceTimer.getContentPane().add(panel_1);
      
      JScrollPane scrollPane_1 = new JScrollPane();
      scrollPane_1.setBounds(10, 21, 157, 169);
      panel_1.add(scrollPane_1);
      
      final JTextArea txtResult = new JTextArea();
      scrollPane_1.setViewportView(txtResult);
      txtResult.setEditable(false);
      
      
      
      txtAverage = new JTextField();
      txtAverage.setEditable(false);
      txtAverage.setBounds(63, 201, 104, 20);
      panel_1.add(txtAverage);
      txtAverage.setColumns(10);
      
      JLabel lblAverage = new JLabel("Average:");
      lblAverage.setBounds(10, 204, 61, 14);
      panel_1.add(lblAverage);
      
      final JLabel lblHours = new JLabel("00");
      lblHours.setFont(new Font("Candara", Font.BOLD, 70));
      lblHours.setHorizontalAlignment(SwingConstants.CENTER);
      lblHours.setBackground(Color.BLACK);
      lblHours.setForeground(Color.BLACK);
      lblHours.setBorder( new LineBorder(null, 0) );
      lblHours.setBounds(10, 11, 110, 78);
      frmPerformanceTimer.getContentPane().add(lblHours);
      
      final JLabel lblmin = new JLabel("00");
      lblmin.setHorizontalAlignment(SwingConstants.CENTER);
      lblmin.setForeground(Color.BLACK);
      lblmin.setFont(new Font("Candara", Font.BOLD, 70));
      lblmin.setBorder(new LineBorder(null, 0));
      lblmin.setBackground(Color.BLACK);
      lblmin.setBounds(130, 11, 110, 78);
      frmPerformanceTimer.getContentPane().add(lblmin);
      
      final JLabel lblsec = new JLabel("00");
      lblsec.setHorizontalAlignment(SwingConstants.CENTER);
      lblsec.setForeground(Color.BLACK);
      lblsec.setFont(new Font("Candara", Font.BOLD, 70));
      lblsec.setBorder(new LineBorder(null, 0));
      lblsec.setBackground(Color.BLACK);
      lblsec.setBounds(250, 11, 110, 78);
      frmPerformanceTimer.getContentPane().add(lblsec);
      
      final JLabel lblms = new JLabel("000");
      lblms.setHorizontalAlignment(SwingConstants.CENTER);
      lblms.setForeground(Color.BLACK);
      lblms.setFont(new Font("Candara", Font.BOLD, 50));
      lblms.setBorder(new LineBorder(null, 0));
      lblms.setBackground(Color.BLACK);
      lblms.setBounds(370, 11, 106, 78);
      frmPerformanceTimer.getContentPane().add(lblms);
      
      
      final JButton btnStart = new JButton("Start");
      btnStop = new JButton("Stop");
      final JButton btnLap = new JButton("Lap");
      final JButton btnReset = new JButton("Reset");
      
      
      btnStart.setMnemonic( KeyEvent.VK_F1 );
      btnStop.setMnemonic( KeyEvent.VK_F12 );
      btnLap.setMnemonic( KeyEvent.VK_F2 );
      btnReset.setMnemonic( KeyEvent.VK_F11 );
         
      
      btnStop.addActionListener(new ActionListener() {
         public void actionPerformed(ActionEvent arg0) {
            String time = lblHours.getText().toString() + ":" + lblmin.getText().toString() + ":" + lblsec.getText().toString() + "." + lblms.getText().toString();
            txtTotal.setText( time );
            txtResult.append( time + "\n" );
            btnStart.setEnabled( true );
            btnStop.setEnabled( false );
            btnLap.setEnabled( false );
            stopped = true;
            
            counter ++;
            
            long milliseconds = (TimeUnit.SECONDS.toMillis(TimeUnit.HOURS.toSeconds(hours) + TimeUnit.MINUTES.toSeconds(minutes)) + TimeUnit.SECONDS.toMillis( seconds )) + ms;
                        
            content.add( milliseconds );
            long sum = 0;
            for (int x = 0; x < content.size(); x++)
            {
               sum += content.get( x );
            }
            
            resultMin = sum / content.size();
                        
            SimpleDateFormat formatter = new SimpleDateFormat("HH:mm:ss.SSS");
            Date date = new Date(resultMin);
            formatter.setTimeZone(TimeZone.getTimeZone("GMT"));
            String returnFormat = formatter.format(date);
            
            txtAverage.setText( returnFormat );

            
         }
      });
      btnStop.setBounds(10, 153, 110, 42);
      frmPerformanceTimer.getContentPane().add(btnStop);
      
      
      btnLap.addActionListener(new ActionListener() {
         public void actionPerformed(ActionEvent arg0) {
           String time = lblHours.getText().toString() + ":" + lblmin.getText().toString() + ":" + lblsec.getText().toString() + "." + lblms.getText().toString();
           
           txtLap.append( time + "\n");
           
         }
      });
      btnLap.setBounds(8, 206, 110, 42);
      frmPerformanceTimer.getContentPane().add(btnLap);
      
      
      btnReset.addActionListener(new ActionListener() {
         public void actionPerformed(ActionEvent arg0) {
            
            stopped = true;
            
            lblHours.setText("00");
            lblmin.setText("00");
            lblsec.setText("00");
            lblms.setText("000");
            
            txtLap.setText( "" );
            txtResult.setText( "" );
            txtTotal.setText( "" );
            txtAverage.setText( "" );
            
            btnStop.setEnabled( false );
            btnLap.setEnabled(false);
            btnReset.setEnabled( false );
            btnStart.setEnabled( true );
            
            
            
         }
      });
      btnReset.setBounds(8, 259, 110, 42);
      frmPerformanceTimer.getContentPane().add(btnReset);
      
      
      btnStart.addActionListener(new ActionListener() {
         public void actionPerformed(ActionEvent arg0) {
            
            btnStart.setEnabled( false );
            btnStop.setEnabled( true );
            btnLap.setEnabled( true );
            btnReset.setEnabled( true );
            final SwingWorker<Void,Void> worker = new SwingWorker<Void,Void>()
            {
                @Override
                protected Void doInBackground()
                {
                   stopped = false;
                   txtLap.setText( "" );
                   
                   DecimalFormat f = new DecimalFormat("00");
                   DecimalFormat fm = new DecimalFormat("000");
                   
                   
                   
                   for(hours = 0; hours < 24; hours++)
                   {                         
                       for(minutes = 0; minutes < 60; minutes++)
                       {
                          for(seconds = 0; seconds < 60; seconds++)
                          { 
                             for(ms = 0; ms < 1000; ms++)
                             {  
                                   if( stopped == false)
                                   {
                                      try
                                      {
                                         lblHours.setText(f.format( hours ));
                                         lblmin.setText(f.format( minutes ));
                                         lblsec.setText(f.format( seconds ));
                                         lblms.setText(fm.format( ms ));
                                         Thread.sleep( 1 ); 
                                      }
                                      catch( InterruptedException e )
                                      {
                                         // TODO Auto-generated catch block
                                         e.printStackTrace();
                                      }
                                   }
                                   else
                                   {
                                      break;
                                   }
                                }                               
                             }
                          }
                       }
                                      
                                      
                        return null;
                }
             
                @Override
                protected void done()
                {
                   
                }
            };
            worker.execute();
         }
         
      });
      btnStart.setBounds(10, 100, 110, 42);
      frmPerformanceTimer.getContentPane().add(btnStart);
      
      JLabel label_1 = new JLabel(":");
      label_1.setHorizontalAlignment(SwingConstants.CENTER);
      label_1.setForeground(Color.BLACK);
      label_1.setFont(new Font("Candara", Font.BOLD, 70));
      label_1.setBorder(new LineBorder(null, 0));
      label_1.setBackground(Color.BLACK);
      label_1.setBounds(74, 11, 110, 78);
      frmPerformanceTimer.getContentPane().add(label_1);
      
      JLabel label_2 = new JLabel(":");
      label_2.setHorizontalAlignment(SwingConstants.CENTER);
      label_2.setForeground(Color.BLACK);
      label_2.setFont(new Font("Candara", Font.BOLD, 70));
      label_2.setBorder(new LineBorder(null, 0));
      label_2.setBackground(Color.BLACK);
      label_2.setBounds(194, 11, 110, 78);
      frmPerformanceTimer.getContentPane().add(label_2);
      
      JLabel label_3 = new JLabel(".");
      label_3.setHorizontalAlignment(SwingConstants.CENTER);
      label_3.setForeground(Color.BLACK);
      label_3.setFont(new Font("Candara", Font.BOLD, 70));
      label_3.setBorder(new LineBorder(null, 0));
      label_3.setBackground(Color.BLACK);
      label_3.setBounds(314, 11, 110, 78);
      frmPerformanceTimer.getContentPane().add(label_3);
      
      btnStop.setEnabled( false );
      btnLap.setEnabled(false);
      btnReset.setEnabled( false );
            
   }
}
