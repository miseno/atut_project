package com.lexmark.es.loggertool.controller;

import javax.swing.JPanel;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JTextField;
import javax.swing.DefaultListModel;
import javax.swing.JButton;
import javax.swing.JFileChooser;

import java.awt.Font;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.List;

import javax.swing.JSeparator;
import javax.swing.JSplitPane;
import javax.swing.border.LineBorder;
import javax.swing.event.ListSelectionListener;

import org.apache.commons.net.ftp.FTP;
import org.apache.commons.net.ftp.FTPClient;

import java.awt.Color;
import java.awt.event.ActionEvent;
import javax.swing.JTable;

public class FTPAppsFirmPanel extends JPanel implements ActionListener{
	/**
	 * 
	 */
	
	private JList<String> countryList;
	private DefaultListModel<String> listModel;
    
	
	private static final long serialVersionUID = 1L;
	private JTextField txtIPAddress;
	private JButton btnAdd, btnReset, btnBrowse, btnDeploy;
	private String[] columns = {"IP Address","Firmware"};
	private Object[][] data;
	private JLabel lblFirmware;
	private JTextField txtFirmwareFile;
	private JFileChooser fc;
	private File dir, file;
	private String filename;
	private JLabel lblStatus;
	boolean noError;
	
	/**
	 * Create the panel.
	 */
	public FTPAppsFirmPanel() {
		setLayout(null);
		
		
		fc = new JFileChooser();
		
		JLabel lblNewLabel = new JLabel("IP Addresses:");
		lblNewLabel.setBounds(10, 105, 111, 14);
		add(lblNewLabel);
		
		txtIPAddress = new JTextField();
		txtIPAddress.setBounds(10, 128, 641, 20);
		add(txtIPAddress);
		txtIPAddress.setColumns(10);
		
		btnAdd = new JButton("ADD");
		btnAdd.addActionListener(this);
		btnAdd.setBounds(661, 125, 135, 23);
		add(btnAdd);
		
		btnReset = new JButton("RESET");
		btnReset.setEnabled(false);
		btnReset.addActionListener(this);
		btnReset.setBounds(661, 184, 135, 23);
		add(btnReset);
		
		btnBrowse = new JButton("BROWSE");
		btnBrowse.addActionListener(this);
		btnBrowse.setBounds(661, 71, 135, 23);
		add(btnBrowse);
		
		btnDeploy = new JButton("DEPLOY");
		btnDeploy.setEnabled(false);
		btnDeploy.addActionListener(this);
		btnDeploy.setBounds(661, 218, 135, 23);
		add(btnDeploy);
		
		
		JLabel lblInstallOrUpdate = new JLabel("Install or Update Firmware");
		lblInstallOrUpdate.setFont(new Font("Tahoma", Font.PLAIN, 14));
		lblInstallOrUpdate.setBounds(10, 11, 312, 20);
		add(lblInstallOrUpdate);
		
		JSeparator separator = new JSeparator();
		separator.setBounds(10, 42, 786, 2);
		add(separator);
		
		JLabel lblListIP = new JLabel("List of IP Addresses");
		lblListIP.setBounds(10, 159, 111, 14);
		add(lblListIP);
		
		listModel = new DefaultListModel<>();
//		listModel.addElement("USA");
//		listModel.addElement("India");
//		listModel.addElement("Vietnam");
//		listModel.addElement("Canada");
//		listModel.addElement("Denmark");
//		listModel.addElement("France");
//		listModel.addElement("Great Britain");
//		listModel.addElement("Japan");
		 
		//create the list
		countryList = new JList<>(listModel);
		countryList.setVisibleRowCount(11);
		countryList.setLayoutOrientation(JList.HORIZONTAL_WRAP);
		countryList.setBorder(new LineBorder(new Color(0, 0, 0)));
		countryList.setLocation(10, 186);
		add(countryList);
		
		countryList.setSize(641,200);
		countryList.setVisible(true);
		
		lblFirmware = new JLabel("Firmware(FLS):");
		lblFirmware.setBounds(10, 55, 111, 14);
		add(lblFirmware);
		
		txtFirmwareFile = new JTextField();
		txtFirmwareFile.setColumns(10);
		txtFirmwareFile.setBounds(10, 74, 641, 20);
		add(txtFirmwareFile);
		
		lblStatus = new JLabel("Status...");
		lblStatus.setBounds(10, 397, 641, 20);
		add(lblStatus);
	

	}
	
	public void actionPerformed(ActionEvent e) {
		String ipAddresses;
		String[] ipAddress;
		int size;
		
		if(e.getSource() == btnAdd) {
			ipAddresses = txtIPAddress.getText();
			
			ipAddress = ipAddresses.split(";");
			
			for(String ip:ipAddress) {
				listModel.addElement(ip);
			}
			
			size = listModel.getSize();
			
			if(size > 0) {
				enable(true);
			}
			
			//System.out.println(listModel.get(1).toString());
			//System.out.println(Integer.toString(size));
			clear();
			
		} else if(e.getSource() == btnReset) {
			
			listModel.clear();
			
			size = listModel.getSize();
			
			if(size == 0) {
				enable(false);
			}
			
		} else if(e.getSource() == btnBrowse) {
			int returnVal = fc.showOpenDialog(btnBrowse);
			
			
			if (returnVal == JFileChooser.APPROVE_OPTION) {
				dir = fc.getCurrentDirectory();
				file = fc.getSelectedFile();
				filename = dir.getAbsolutePath().toString().concat("\\").concat(file.getName().toString());
				txtFirmwareFile.setText(filename);
			}
		} else if(e.getSource() == btnDeploy) {
			
			
			
			//Use threading to set the label
			Thread t = new Thread(new Runnable() {
		        @Override
		        public void run() {
		        	lblStatus.setText("Start uploading the file...");
		        	enable(false);
		        	
		            //Do the job
		        	FTPClient ftpClient = new FTPClient();
					//lblStatus.setText("Start uploading the file...");
					try {
						
						//String machine = new String(txtMachineAddress.getText().toString());
						String machine;
						for(int i=0; i<listModel.getSize(); i++) {
							
						
							machine = listModel.get(i).toString();
							
							//lblStatus.setText(machine.toString());
							
							ftpClient.connect(machine, 21);	
							ftpClient.login("", "");
							ftpClient.enterLocalPassiveMode();
							
							ftpClient.setFileType(FTP.BINARY_FILE_TYPE);
							
							File firstLocalFile = new File(filename.toString());
							String firstRemoteFile = file.toString();
							InputStream inputStream = new FileInputStream(firstLocalFile);
							
							
							
							OutputStream outputStream = ftpClient.storeFileStream(firstRemoteFile);
							byte[] bytesIn = new byte[4096];
				            int read = 0;
				 
				            while ((read = inputStream.read(bytesIn)) != -1) {
				                outputStream.write(bytesIn, 0, read);
				            }
				            inputStream.close();
				            outputStream.close();
				 
				            boolean completed = ftpClient.completePendingCommand();
				            if (completed) {
				            	noError = true;
				            } else {
				            	noError = false;
				            }
			            
						}
						
					} catch (IOException ex) {
			            System.out.println("Error: " + ex.getMessage());
			            ex.printStackTrace();
			        }
		            //myLabel.setText("Processed");
					if(noError) {
						lblStatus.setText("Uploading completed...");
		            	enable(true);
					}
		        }     
		    });
		    t.start();
		}
		
	}
	
	public void clear() {
		txtIPAddress.setText("");
	}
	
	public void enable(boolean state) {
		btnReset.setEnabled(state);
		btnDeploy.setEnabled(state);
	}
}
