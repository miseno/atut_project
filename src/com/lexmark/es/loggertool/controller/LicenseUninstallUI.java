package com.lexmark.es.loggertool.controller;

import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JList;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JProgressBar;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import javax.swing.JButton;
import javax.swing.JDialog;

import java.awt.event.ActionListener;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.awt.event.ActionEvent;
import javax.swing.JLabel;
import java.awt.Font;
import javax.swing.JTextField;
import javax.swing.SwingWorker;

import java.awt.Toolkit;

public class LicenseUninstallUI
{

   public static JFrame frmLicenseUninstallTool;
   private JTextField txtIPAddress;
   private JTextField txtLicID;

   /**
    * Launch the application.
    */
   public static void main( String[] args )
   {
      EventQueue.invokeLater( new Runnable()
      {
         public void run()
         {
            try
            {
               LicenseUninstallUI window = new LicenseUninstallUI();
               window.frmLicenseUninstallTool.setVisible( true );
            }
            catch( Exception e )
            {
               e.printStackTrace();
            }
         }
      } );
   }

   /**
    * Create the application.
    */
   public LicenseUninstallUI()
   {
      initialize();
   }

   /**
    * Initialize the contents of the frame.
    */
   private void initialize()
   {
      frmLicenseUninstallTool = new JFrame();
      frmLicenseUninstallTool.setTitle("License Uninstall Tool");
      frmLicenseUninstallTool.setIconImage(Toolkit.getDefaultToolkit().getImage("C:\\Users\\miseno\\Downloads\\images\\images.png"));
      frmLicenseUninstallTool.setBounds( 100, 100, 546, 121 );
      frmLicenseUninstallTool.setDefaultCloseOperation( JFrame.DISPOSE_ON_CLOSE );
      frmLicenseUninstallTool.getContentPane().setLayout(null);
      
      JLabel label = new JLabel("Printer IP Address");
      label.setFont(new Font("Tahoma", Font.BOLD, 13));
      label.setBounds(10, 17, 126, 14);
      frmLicenseUninstallTool.getContentPane().add(label);
      
      JButton btnUninstallLicense = new JButton("Remove License");
      btnUninstallLicense.addActionListener(new ActionListener() {
         public void actionPerformed(ActionEvent arg0) {
            
            final JDialog dialog = new JDialog(); // modal
            dialog.setUndecorated(true);
            JProgressBar bar = new JProgressBar();
            bar.setIndeterminate(true);
            bar.setStringPainted(true);
            bar.setString("Please wait");
            JPanel panel = new JPanel();
            panel.add( bar );
            dialog.setLocationRelativeTo(null);
            dialog.getContentPane().add(panel);
            dialog.pack();
            
            SwingWorker<Void,Void> worker = new SwingWorker<Void,Void>()
            {
                @Override
                protected Void doInBackground()
                {
            
                        int respCode = ResponseController.Status( txtIPAddress.getText().toString().trim() );
            
            if (!txtIPAddress.getText().toString().trim().equals( "" ))
            {
               if (!txtLicID.getText().toString().trim().equals( "" ))
               {
               Runtime rt = Runtime.getRuntime();
               
               String curlCommand = "curl -XDELETE -F \"id=" + txtLicID.getText().toString().trim() + "\" http://" + txtIPAddress.getText().toString().trim() + "/webservices/vcc/licenses";
               String s = null;
               try
               {
                  Process pr = rt.exec(curlCommand);
                  
                  BufferedReader stdInput = new BufferedReader(new 
                  InputStreamReader(pr.getInputStream()));
                  
                  while ((s = stdInput.readLine()) != null) {
                      JOptionPane.showMessageDialog( null, s );
                  }
               }
               catch( IOException e1 )
               {
                  // TODO Auto-generated catch block
                  e1.printStackTrace();
               }  
               }
               else
               {
                  JOptionPane.showMessageDialog( null, "Please Select a License file to Install" );
               }
            }
            else
            {
               JOptionPane.showMessageDialog( null, "Please enter the Device IP Address you wish the license to install" );
            }
            
            return null;
         }
      
         @Override
         protected void done()
         {
             dialog.dispose();
         }
     };
     worker.execute();
     dialog.setVisible(true);
         }
      });
      btnUninstallLicense.setBounds(325, 17, 152, 47);
      frmLicenseUninstallTool.getContentPane().add(btnUninstallLicense);
      
      txtIPAddress = new JTextField();
      txtIPAddress.setColumns(10);
      txtIPAddress.setBounds(146, 15, 171, 20);
      frmLicenseUninstallTool.getContentPane().add(txtIPAddress);
      
      JLabel lblLicenseId = new JLabel("License ID");
      lblLicenseId.setFont(new Font("Tahoma", Font.BOLD, 13));
      lblLicenseId.setBounds(10, 44, 126, 14);
      frmLicenseUninstallTool.getContentPane().add(lblLicenseId);
      
      txtLicID = new JTextField();
      txtLicID.setColumns(10);
      txtLicID.setBounds(87, 42, 230, 20);
      frmLicenseUninstallTool.getContentPane().add(txtLicID);
      
      JButton btnNewButton = new JButton("?");
      btnNewButton.addActionListener(new ActionListener() {
         public void actionPerformed(ActionEvent e) {
            licenses lic = new licenses();
            
            if (!txtIPAddress.getText().toString().trim().isEmpty())
            {
            lic.parseLicense( txtIPAddress.getText().toString().trim() );
            }
            else
            {
               JOptionPane.showMessageDialog( null, "Please enter an IP Address" );
            }
         }
      });
      btnNewButton.setBounds(482, 17, 38, 47);
      frmLicenseUninstallTool.getContentPane().add(btnNewButton);
   }
}
