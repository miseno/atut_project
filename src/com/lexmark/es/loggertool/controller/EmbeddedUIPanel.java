package com.lexmark.es.loggertool.controller;

import javax.swing.JPanel;
import javax.swing.JProgressBar;
import javax.swing.JButton;
import java.awt.GridLayout;
import java.awt.Toolkit;

import javax.swing.JLabel;
import javax.swing.JOptionPane;

import java.awt.Font;
import javax.swing.JTextField;
import java.awt.GridBagLayout;
import java.awt.GridBagConstraints;
import java.awt.FlowLayout;
import javax.swing.SpringLayout;
import net.miginfocom.swing.MigLayout;
import javax.swing.SwingConstants;
import javax.swing.SwingWorker;
import javax.swing.border.TitledBorder;
import javax.swing.UIManager;
import java.awt.Color;
import java.awt.Desktop;
import java.awt.Dimension;

import javax.swing.JCheckBox;
import javax.swing.JDialog;
import javax.swing.JFileChooser;
import javax.swing.JRadioButton;
import javax.swing.JTextArea;
import javax.script.ScriptException;
import javax.swing.ButtonGroup;
import javax.swing.ImageIcon;
import java.awt.event.ActionListener;
import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.awt.event.ActionEvent;

public class EmbeddedUIPanel extends JPanel
{
   private JTextField txtIPAddress;
   private JTextField txtFilename;
   private JTextField txtDirectory;
   private JButton btnPrinterStatus;
   private JButton btnBrowse;
   private JButton btnClear;
   private JButton btnCaptureAll;
   private JButton btnSet;
   private JLabel lblStatusMessage;
   static File file;
   static BufferedReader in;
   static PrintWriter writer;
   int respCode;   
   
  /**
    * Create the panel.
    */
   public EmbeddedUIPanel()
   {
      
      
      setLayout(null);
      
      
      JLabel label = new JLabel("IP ADDRESS");
      label.setFont(new Font("Tahoma", Font.BOLD, 13));
      label.setBounds(10, 16, 81, 14);
      add(label);
      
      txtIPAddress = new JTextField();
      txtIPAddress.setColumns(10);
      txtIPAddress.setBounds(109, 14, 208, 20);
      add(txtIPAddress);
      
      JLabel label_1 = new JLabel("FOLDER NAME");
      label_1.setFont(new Font("Tahoma", Font.BOLD, 13));
      label_1.setBounds(10, 47, 107, 14);
      add(label_1);
      
      txtFilename = new JTextField();
      txtFilename.setColumns(10);
      txtFilename.setBounds(108, 45, 209, 20);
      add(txtFilename);
      
      JPanel panel = new JPanel();
      panel.setLayout(null);
      panel.setBorder(new TitledBorder(UIManager.getBorder("TitledBorder.border"), "Logs", TitledBorder.LEADING, TitledBorder.TOP, null, new Color(0, 0, 0)));
      panel.setBounds(171, 72, 146, 113);
      add(panel);
      
      final JCheckBox chckbxFirmwareDebug = new JCheckBox("Firmware Debug");
      chckbxFirmwareDebug.setSelected(true);
      chckbxFirmwareDebug.setBounds(21, 75, 119, 23);
      panel.add(chckbxFirmwareDebug);
      
      final JCheckBox chckbxBundleInfo = new JCheckBox("Bundle Info");
      chckbxBundleInfo.setSelected(true);
      chckbxBundleInfo.setBounds(21, 49, 97, 23);
      panel.add(chckbxBundleInfo);
      
      final JCheckBox chckbxSeLogs = new JCheckBox("SE Logs");
      chckbxSeLogs.setSelected(true);
      chckbxSeLogs.setBounds(21, 23, 97, 23);
      panel.add(chckbxSeLogs);
      
      btnClear = new JButton("Clear SE Logs");
      btnClear.addActionListener(new ActionListener() {
         public void actionPerformed(ActionEvent e) {
                        
            Thread t = new Thread(new Runnable() {
               @Override
               public void run() {
                 lblStatusMessage.setText("Clearing SE Logs..Please Wait..");
                 ButtonState(false);
                       
                       try
                       {
                          int respCode = ResponseController.Status( getIPAddress() );
                          
                          if (respCode != 200)
                          {
                             // break if respCode is not 200
                          }
                          else
                          {
                          Clear.ClearSELOG( getIPAddress() );
                          }
                       }
                       catch( ScriptException e1 )
                       {
                          // TODO Auto-generated catch block
                          e1.printStackTrace();
                       }
                 
                 ButtonState(true);
                 lblStatusMessage.setText("Successfully Cleared...");
               }     
           });
           t.start();
            
         }
      });
      btnClear.setBounds(10, 130, 151, 55);
      add(btnClear);
      
      btnCaptureAll = new JButton("Capture");
      btnCaptureAll.addActionListener(new ActionListener() {
         public void actionPerformed(ActionEvent e) {
            
            Thread t = new Thread(new Runnable() {
               @Override
               public void run() {
                 lblStatusMessage.setText("Capturing Logs... Please Wait...");
                 ButtonState(false);
                 respCode = ResponseController.Status( getIPAddress() );
                 
                 if (getIPAddress().isEmpty() && getFolderName().isEmpty() && getDirectory().isEmpty())
                 {
                    JOptionPane.showOptionDialog(null, "Missing Information Detected! Please check IP Address, Folder Name and Directory Fields", "Missing Information", JOptionPane.OK_OPTION, JOptionPane.ERROR_MESSAGE, null, null, null);
                 }   
                 else if (getIPAddress().equals( "" ))
                 {
                    JOptionPane.showOptionDialog(null, "Please enter an IP Address", "Missing Information", JOptionPane.OK_OPTION, JOptionPane.ERROR_MESSAGE, null, null, null);
                 }
                 else if (getFolderName().equals( "" ))
                 {
                    JOptionPane.showOptionDialog(null, "Please enter a folder name.", "Missing Information", JOptionPane.OK_OPTION, JOptionPane.ERROR_MESSAGE, null, null, null);
                 }
                 else if (getDirectory().equals( "" ))
                 {
                    JOptionPane.showOptionDialog(null, "Please select the directory where to save the file.", "Missing Information", JOptionPane.OK_OPTION, JOptionPane.ERROR_MESSAGE, null, null, null);
                 }                               
                 else if (!chckbxFirmwareDebug.isSelected() && !chckbxBundleInfo.isSelected() && !chckbxSeLogs.isSelected())
                 {
                    JOptionPane.showOptionDialog(null, "Please select the log to save.", "Missing Information", JOptionPane.OK_OPTION, JOptionPane.ERROR_MESSAGE, null, null, null);
                 }
                 else if (respCode != 200)
                 {
                    // break if respCode is not 200                                
                 }
                 else
                 {
                    try
                    {
                       System.out.println( respCode );
                       Object[] Options = { "Open File Location", "OK"};
                       
                       String directoryLocale = Capture.dirCreate( getFolderName(), getDirectory() );
                       
                                                                
                       if (chckbxFirmwareDebug.isSelected())
                       {
                          Capture.getFirmwareDebug( getIPAddress(), directoryLocale );
                       }
                       
                       if (chckbxBundleInfo.isSelected())
                       {
                          Capture.BundleInfo( getIPAddress(), getFolderName(), directoryLocale );
                       }
                          
                       if (chckbxSeLogs.isSelected())
                       {
                          Capture.Save( getIPAddress(), getFolderName(), directoryLocale );
                       }
                       
                                               
                       int result = JOptionPane.showOptionDialog(null, "Download Complete!","Success!", JOptionPane.YES_NO_OPTION, JOptionPane.INFORMATION_MESSAGE, null, Options, null);
                       
                       if (result == JOptionPane.YES_OPTION){
                          Desktop desktop = Desktop.getDesktop();
                          
                          File dirToOpen = null;
                          try {
                             
                             dirToOpen = new File(directoryLocale);
                             desktop.open(dirToOpen);
                             
                                                   
                             txtFilename.setText( "" );
                             txtDirectory.setText( "" );
                             
                          } catch (IllegalArgumentException iae) {
                              iae.printStackTrace();
                          }
                          catch( IOException e1 )
                          {
                             // TODO Auto-generated catch block
                             e1.printStackTrace();
                          }
                       }
                       
                       if (result == JOptionPane.NO_OPTION){                     
                          txtFilename.setText( "" );
                       }
                       
                       
                    }
                    catch( ScriptException e1 )
                    {
                       // TODO Auto-generated catch block
                       e1.printStackTrace();
                    }
                 }
                 ButtonState(true);
                 lblStatusMessage.setText("Capturing of Logs is complete...");
               }     
           });
           t.start();
            
            
         }
      });
      btnCaptureAll.setBounds(10, 72, 151, 55);
      add(btnCaptureAll);
      
      JPanel panel_1 = new JPanel();
      panel_1.setLayout(null);
      panel_1.setBorder(new TitledBorder(null, "Logging Options", TitledBorder.LEADING, TitledBorder.TOP, null, null));
      panel_1.setBounds(10, 191, 307, 131);
      add(panel_1);
      
      final JRadioButton rdbtnYes = new JRadioButton("Yes");
      rdbtnYes.setSelected(true);
      rdbtnYes.setBounds(6, 22, 104, 23);
      panel_1.add(rdbtnYes);
      
      final JRadioButton rdbtnNo = new JRadioButton("No");
      rdbtnNo.setBounds(6, 47, 104, 23);
      panel_1.add(rdbtnNo);
      
      final JRadioButton rdbtnDefault = new JRadioButton("Default");
      rdbtnDefault.setBounds(6, 73, 104, 23);
      panel_1.add(rdbtnDefault);
      
      final JLabel lblLoggingLevel = new JLabel("Logging Level: ");
      lblLoggingLevel.setFont(new Font("Tahoma", Font.BOLD, 12));
      lblLoggingLevel.setBounds(114, 102, 183, 18);
      panel_1.add(lblLoggingLevel);
      
      final ButtonGroup group = new ButtonGroup();
      group.add(rdbtnYes);
      group.add(rdbtnNo);
      group.add(rdbtnDefault);
      
      btnSet = new JButton("Set");
      btnSet.addActionListener(new ActionListener() {
         public void actionPerformed(ActionEvent e) {
            
            Thread t = new Thread(new Runnable() {
               @Override
               public void run() {
                 lblStatusMessage.setText("Setting Logging Level...");
                 ButtonState(false);
                 respCode = ResponseController.Status( getIPAddress() );
                 
                 lblLoggingLevel.setText( "Logging Level: " );
                 
                 if ( getIPAddress().isEmpty())
                 {
                    JOptionPane.showOptionDialog(null, "Please enter an IP Address", "Missing Information", JOptionPane.OK_OPTION, JOptionPane.ERROR_MESSAGE, null, null, null);
                 }
                 else if (respCode != 200)
                 {
                    // break if respCode is not 200
                 }
                 else
                 {
                    if (rdbtnYes.isSelected() == true)
                    {
                       try
                       {
                          SetLoggingLevel.logYes( getIPAddress() );
                          lblLoggingLevel.setText( "Logging Level: Yes" );
                          lblLoggingLevel.setForeground( Color.BLUE );
                       }
                       catch( ScriptException e )
                       {
                          // TODO Auto-generated catch block
                          e.printStackTrace();
                       }
                    }
                    
                    else if (rdbtnNo.isSelected() == true)
                    {
                       try
                       {
                          SetLoggingLevel.logNo( getIPAddress() );
                          lblLoggingLevel.setText( "Logging Level: No" );
                          lblLoggingLevel.setForeground( Color.RED );
                       }
                       catch( ScriptException e )
                       {
                          // TODO Auto-generated catch block
                          e.printStackTrace();
                       }
                    }
                    
                    if (rdbtnDefault.isSelected() == true)
                    {
                       try
                       {
                          SetLoggingLevel.logDefault( getIPAddress() );
                          lblLoggingLevel.setText( "Logging Level: Default" );
                          lblLoggingLevel.setForeground( Color.BLACK );
                       }
                       catch( ScriptException e )
                       {
                          // TODO Auto-generated catch block
                          e.printStackTrace();
                       }
                    }
                 }
                 ButtonState(true);
                 lblStatusMessage.setText("Setting Successful...");
               } 
                 
           });
           t.start();
            
         }
      });
      btnSet.setBounds(146, 29, 151, 59);
      panel_1.add(btnSet);
      
      txtDirectory = new JTextField();
      txtDirectory.setEnabled(false);
      txtDirectory.setEditable(false);
      txtDirectory.setColumns(10);
      txtDirectory.setBounds(10, 333, 258, 20);
      add(txtDirectory);
      
      btnBrowse = new JButton("...");
      btnBrowse.addActionListener(new ActionListener() {
         public void actionPerformed(ActionEvent e) {
            JFileChooser chooser = new JFileChooser();
            chooser.setCurrentDirectory(new java.io.File("user.home"));
            chooser.setDialogTitle("Select Directory");
            chooser.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
            
            chooser.setAcceptAllFileFilterUsed(false);
            
            if (chooser.showOpenDialog(null) == JFileChooser.APPROVE_OPTION) {                  
               txtDirectory.setText( chooser.getSelectedFile().toString() );                    
               }                    
         }
      });
      btnBrowse.setBounds(273, 332, 49, 23);
      add(btnBrowse);
      
      JPanel panel_2 = new JPanel();
      panel_2.setLayout(null);
      panel_2.setBorder(new TitledBorder(UIManager.getBorder("TitledBorder.border"), "Printer Information", TitledBorder.LEADING, TitledBorder.TOP, null, new Color(0, 0, 0)));
      panel_2.setBounds(332, 11, 503, 342);
      add(panel_2);
      
      final JTextArea txtPrinterInfo = new JTextArea();
      txtPrinterInfo.setEditable(false);
      txtPrinterInfo.setBounds(10, 21, 483, 271);
      panel_2.add(txtPrinterInfo);
      
      lblStatusMessage = new JLabel("");
      lblStatusMessage.setFont(new Font("Tahoma", Font.BOLD, 13));
      lblStatusMessage.setBounds(10, 364, 825, 25);
      add(lblStatusMessage);
      
      btnPrinterStatus = new JButton("Display Printer Information");
      btnPrinterStatus.addActionListener(new ActionListener() {
         public void actionPerformed(ActionEvent arg0) {
            
            Thread t = new Thread(new Runnable() {
               @Override
               public void run() {
                 lblStatusMessage.setText("Uploading Printer Information...");
                 
                    String info;
                    try
                    {
                       ButtonState(false);
                       info = Capture.ShowInfo( getIPAddress() );
                       txtPrinterInfo.setText( "" );
                       txtPrinterInfo.setText( info );
                       ButtonState(true);
                    }
                    catch( Exception e )
                    {
                       // TODO Auto-generated catch block
                       e.printStackTrace();
                    }                 
                 
                 lblStatusMessage.setText("Uploading completed...");
               }     
           });
           t.start();
         }
      });
      btnPrinterStatus.setBounds(10, 303, 197, 28);
      panel_2.add(btnPrinterStatus);
      
              
   }
   
   
   
   public String getIPAddress()
   {
      String ipAddress = txtIPAddress.getText().toString().trim();
      
      return ipAddress;
   }
   
   public String getFolderName()
   {
      String folderName = txtFilename.getText().toString().trim();
            
      return folderName;
   }
   
   public String getDirectory()
   {
      String directory = txtDirectory.getText().toString().trim();
            
      return directory;
   }
   public void ButtonState(Boolean state)
   {
      btnPrinterStatus.setEnabled( state );
      btnBrowse.setEnabled( state );
      btnClear.setEnabled( state );
      btnCaptureAll.setEnabled( state );
      btnSet.setEnabled( state );
   }
}
