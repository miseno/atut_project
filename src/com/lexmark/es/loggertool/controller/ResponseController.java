package com.lexmark.es.loggertool.controller;

import java.net.HttpURLConnection;
import java.net.URL;

import javax.swing.JOptionPane;

public class ResponseController
{
    public static int Status(String ipAdd)
    {
       int code = 0;
       try
       {
       URL url = new URL("http://" + ipAdd);
       HttpURLConnection connection = (HttpURLConnection)url.openConnection();
       connection.setRequestMethod("GET");
       connection.connect();

       code = connection.getResponseCode();
       }
       catch (Exception e)
       {
          JOptionPane.showMessageDialog( null, "Cannot Connect to " + ipAdd );
       }
       
       return code;
       
    }
    
    public static String FamilyClassify(String ipAdd)
    {
       String family = "";
       int code = 0;
       try
       {
          String legacy = "http://" + ipAdd + "/cgi-bin/script/printer/prtappse";
          String moja = "http:// " + ipAdd + "cgi-bin/prtappse";
          
          URL url = new URL( legacy );
          HttpURLConnection connection = (HttpURLConnection)url.openConnection();
          connection.setRequestMethod("GET");
          connection.connect();
   
          connection.getResponseCode();
          System.out.println( connection.getResponseCode() );
          
          if (connection.getResponseCode() == 200)
          {
             family = "LEGACY";
          }
          else if (connection.getResponseCode() == 404)
          {
             family = "MOJA";
          }     
          
          }
          catch (Exception e)
          {
             JOptionPane.showMessageDialog( null, "Cannot Connect to " + ipAdd );
          }
       
       return family;
       
    }
}
